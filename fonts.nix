{ pkgs, ... }:

{
  fonts = {
    enableDefaultPackages = true;

    packages = with pkgs; [
      font-awesome
      nerd-fonts.fantasque-sans-mono
      noto-fonts
      noto-fonts-cjk-sans
      noto-fonts-color-emoji
      roboto-mono
    ];

    fontconfig = {
      defaultFonts = {
        monospace = [ "Fantasque Sans M Nerd Font Mono" "Roboto Mono" ];
        emoji = ["Noto Color Emoji"];
      };
      useEmbeddedBitmaps = true;
    };

    fontDir.enable = true;
  };
}
