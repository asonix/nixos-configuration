# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ pkgs, config, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./wireguard.nix
    ];

  # Nix cache
  nix.extraOptions = ''
  secret-key-files = /etc/nix/cache-priv-key.pem
  '';

  hardware.keyboard.qmk.enable = true;

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.kernelPackages = pkgs.linuxPackages_latest;

  boot.initrd.luks.devices."luks-f2fdbf84-2c4b-47d5-b663-dec3abb173c9".device = "/dev/disk/by-uuid/f2fdbf84-2c4b-47d5-b663-dec3abb173c9";

  boot.extraModulePackages = with config.boot.kernelPackages; [ v4l2loopback xpadneo ];

  boot.kernelParams = [ "mem_sleep_default=s2idle" ];
  systemd.sleep.extraConfig = ''
    HibernateDelaySec=30m
    SuspendState=mem
  '';

  # Don't sleep if charging or docked
  services.logind.lidSwitchDocked = "ignore";
  services.logind.lidSwitchExternalPower = "ignore";

  # suspend-then-hibernate if lid closed on battery or power button pressed
  services.logind.lidSwitch = "suspend-then-hibernate";
  services.logind.extraConfig = ''
    HandleSuspendKey=suspend-then-hibernate
  '';

  services.xserver.videoDrivers = ["amdgpu"];

  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  networking.hostName = "squirrelflight"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.11"; # Did you read the comment?

}
