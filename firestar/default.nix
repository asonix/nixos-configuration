# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./btrbk.nix
      ./builder.nix
      ./dual-graphics.nix
      ./hardware-configuration.nix
      ./wireguard.nix
    ];

  # Nix cache
  nix.extraOptions = ''
  secret-key-files = /etc/nix/cache-priv-key.pem
  '';

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.initrd.luks.devices."luks-18cf6425-3540-4121-9313-dc3307d8f42a".device = "/dev/disk/by-uuid/18cf6425-3540-4121-9313-dc3307d8f42a";

  boot.extraModulePackages = with config.boot.kernelPackages; [ v4l2loopback xpadneo ];

  boot.kernelPackages = pkgs.linuxPackages_latest;

  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  fileSystems = {
    "/".options = ["compress=zstd"];
    "/home".options = ["compress=zstd"];
    "/nix".options = ["compress=zstd" "noatime"];
    "/root".options = ["compress=zstd"];
    "/var/log".options = ["compress=zstd"];
    "/home/asonix/Development".options = ["compress=zstd"];
    "/home/asonix/Documents".options = ["compress=zstd"];
    "/home/asonix/Downloads".options = ["compress=zstd"];
    "/home/asonix/Games".options = ["compress=zstd"];
    "/home/asonix/InstantUpload".options = ["compress=zstd"];
    "/home/asonix/Music".options = ["compress=zstd"];
    "/home/asonix/Pictures".options = ["compress=zstd"];
    "/home/asonix/Videos".options = ["compress=zstd"];
  };

  networking.hostName = "firestar"; # Define your hostname.

  services.btrfs.autoScrub = {
    enable = true;
    interval = "weekly";
    fileSystems = [
      "/"
      "/home"
      "/root"
      "/var/log"
      "/home/asonix/Development"
      "/home/asonix/Documents"
      "/home/asonix/Downloads"
      "/home/asonix/Games"
      "/home/asonix/InstantUpload"
      "/home/asonix/Music"
      "/home/asonix/Pictures"
      "/home/asonix/Videos"
    ];
  };

  powerManagement.cpuFreqGovernor = "performance";

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "23.11"; # Did you read the comment?

}
