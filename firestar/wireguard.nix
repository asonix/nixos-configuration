{ config, pkgs, ... }:

{
  networking.firewall = {
    allowedUDPPorts = [ 51820 ];
  };

  networking.wg-quick.interfaces = {
    wg0 = {
      autostart = false;
      address = [
        "192.168.5.10/24"
        "2001:db8:5::10/64"
      ];
      dns = [
        "192.168.5.1"
        "2001:db8:5::1"
      ];
      privateKeyFile = "/etc/wireguard/privatekey";
      peers = [
        {
          publicKey = "v+asDVK/1TMuQCvCWBwUYZpVgj+PGgdhvcvJXOVN0Xo=";
          allowedIPs = [
            "192.168.5.0/24"
            "192.168.20.0/24"
            "2001:db8:5::/64"
            "2001:db8:20::/64"
          ];
          endpoint = "wg.asonix.dog:51820";
          persistentKeepalive = 25;
        }
      ];
    };
  };
}
