{ pkgs, ... }:

let
  volumeConfig = (subvolumes: {
    snapshot_dir = "@snapshots";
    subvolume = builtins.foldl'
      (acc: subvol: acc // {
        ${subvol} = { };
      })
      { }
      subvolumes;
  });
in
{
  services.btrbk = {
    sshAccess = [
      {
        key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHTqU3EvTgY5/e9m6YyQWypQPK58t9iPmPnPYAvnODGB asonix@lionheart";
        roles = [ "source" "info" "send" ];
      }
    ];
    instances = {
      btrbk = {
        onCalendar = "hourly";
        settings = {
          snapshot_preserve_min = "2d";
          snapshot_preserve = "7d 5w";
          transaction_log = "/var/log/btrbk.log";
          volume = {
            "/btrfs/nvme1" = volumeConfig [
              "@"
              "@development"
              "@documents"
              "@downloads"
              "@home"
              "@instantupload"
              "@music"
              "@notes"
              "@photos"
              "@root"
              "@videos"
            ];
          };
        };
      };
      games = {
        onCalendar = "hourly";
	settings = {
	  snapshot_preserve_min = "1h";
	  snapshot_preserve = "2h 2d";
	  transaction_log = "/var/log/btrbk-games.log";
	  volume = {
	    "/btrfs/nvme1" = volumeConfig [ "@games" ];
	  };
	};
      };
    };
  };
}
