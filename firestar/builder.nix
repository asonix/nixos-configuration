{
  users = {
    groups.builder = {};
    users.builder = {
      description = "NixOS Builder user";
      group = "builder";
      isNormalUser = true;
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIvRmuGz5yFAmIRhAOMvbB322aUXaK8Wuc1yqI84fuvM asonix@firestar"
      ];
    };
  };

  nix.settings.trusted-users = [
    "root"
    "builder"
  ];
}
