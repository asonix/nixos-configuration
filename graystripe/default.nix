# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).

{ config, pkgs, ... }:

{
  imports =
    [
      # Include the results of the hardware scan.
      ./btrbk.nix
      ./hardware-configuration.nix
      # ./egpu.nix
      ./wireguard.nix
    ];

  # Nix cache
  # nix.extraOptions = ''
  # secret-key-files = /etc/nix/cache-priv-key.pem
  # '';
  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  boot.kernelPackages = pkgs.linuxPackages_latest;

  # Setup keyfile
  boot.initrd.secrets = {
    "/crypto_keyfile.bin" = null;
  };

  # Enable swap on luks
  boot.initrd.luks.devices."luks-8b65fe7f-2115-422f-9443-7c6265d4d8ea".device = "/dev/disk/by-uuid/8b65fe7f-2115-422f-9443-7c6265d4d8ea";
  boot.initrd.luks.devices."luks-8b65fe7f-2115-422f-9443-7c6265d4d8ea".keyFile = "/crypto_keyfile.bin";

  boot.kernelParams = [ "mem_sleep_default=s2idle" ];
  systemd.sleep.extraConfig = ''
    HibernateDelaySec=30m
    SuspendState=mem
  '';

  # Don't sleep if charging or docked
  services.logind.lidSwitchDocked = "ignore";
  services.logind.lidSwitchExternalPower = "ignore";

  # suspend-then-hibernate if lid closed on battery or power button pressed
  services.logind.lidSwitch = "suspend-then-hibernate";
  services.logind.extraConfig = ''
    HandleSuspendKey=suspend-then-hibernate
  '';

  # should be inferred as swap partition by default
  # boot.resumeDevice = "/dev/disk/by-uuid/8b65fe7f-2115-422f-9443-7c6265d4d8ea";

  # Uncomment if hibernate doesn't work
  # security.protectKernelImage = false;

  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  fileSystems = {
    "/".options = [ "compress=zstd" ];
    "/home".options = [ "compress=zstd" ];
    "/nix".options = [ "compress=zstd" ];
    "/root".options = [ "compress=zstd" ];
  };

  networking.hostName = "graystripe"; # Define your hostname.

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?

}
