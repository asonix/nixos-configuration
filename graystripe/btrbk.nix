{ pkgs, ... }:

let
  volumeConfig = (subvolumes: {
    snapshot_dir = "@snapshots";
    subvolume = builtins.foldl'
      (acc: subvol: acc // {
        ${subvol} = { };
      })
      { }
      subvolumes;
  });
in
{
  services.btrbk = {
    sshAccess = [
      {
        key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHTqU3EvTgY5/e9m6YyQWypQPK58t9iPmPnPYAvnODGB asonix@lionheart";
        roles = [ "source" "info" "send" ];
      }
    ];
    instances.btrbk = {
      onCalendar = "hourly";
      settings = {
        snapshot_preserve_min = "2d";
        snapshot_preserve = "7d 5w";
        transaction_log = "/var/log/btrbk.log";
        volume."/btrfs/nvme" = volumeConfig [
          "@"
          "@home"
          "@root"
        ];
      };
    };
  };
}
