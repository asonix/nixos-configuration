{
  inputs = {
    lix-module = {
      url = "https://git.lix.systems/lix-project/nixos-module/archive/2.92.0.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    niri-flake = {
      url = "git+https://git.asonix.dog/asonix/niri-flake?ref=asonix/pantheon-stuff";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
  };

  outputs = { self, lix-module, niri-flake, nixpkgs, nixos-hardware, ... }@attrs: {
    nixosConfigurations.firestar = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs = attrs;
      modules = [
        lix-module.nixosModules.default
        niri-flake.nixosModules.niri
        nixos-hardware.nixosModules.common-cpu-amd
        nixos-hardware.nixosModules.common-cpu-amd-pstate
        nixos-hardware.nixosModules.common-gpu-amd
        ./desktop.nix
        ./firestar
        ./fonts.nix
        ./lix-cache.nix
        ./niri.nix
        ({ pkgs, ... }: {
           nixpkgs.overlays = [ niri-flake.overlays.niri ];
        })
        ./packages.nix
        (import ./user.nix {
          name = "Aode";
          authorizedKeys = [
            "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILojJubDCB2Mc4fw2mdp5Lzg5mTifXwawIVuyb3vr4lB asonix@graystripe"
            "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICrEHb9Dma3Ig4OAFqvrbZ9jXDrpEGYtBuIBe59lHKxw asonix@squirrelflight"
          ];
        })
        {
          services.openssh = {
            enable = true;
            settings.PermitRootLogin = "no";
          };
        }
      ];
    };
    nixosConfigurations.graystripe = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs = attrs;
      modules = [
        lix-module.nixosModules.default
        niri-flake.nixosModules.niri
      	nixos-hardware.nixosModules.framework-11th-gen-intel
        ./desktop.nix
        ./fonts.nix
        ./graystripe
        ./lix-cache.nix
        ./niri.nix
        ({ pkgs, ... }: {
           nixpkgs.overlays = [ niri-flake.overlays.niri ];
        })
        ./packages.nix
        (import ./user.nix { name = "Tavi"; })
        {
          services.openssh = {
            enable = true;
            settings.PermitRootLogin = "no";
          };
        }
      ];
    };
    nixosConfigurations.squirrelflight = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs = attrs;
      modules = [
        lix-module.nixosModules.default
        niri-flake.nixosModules.niri
        nixos-hardware.nixosModules.framework-16-7040-amd
        ./desktop.nix
        ./fonts.nix
        ./lix-cache.nix
        ({ pkgs, ... }: {
           nixpkgs.overlays = [ niri-flake.overlays.niri ];
        })
        ./packages.nix
        ./niri.nix
        ./squirrelflight
        (import ./user.nix { name = "Tavi"; })
        {
          services.openssh = {
            enable = true;
            settings.PermitRootLogin = "no";
          };
        }
      ];
    };
  };
}
