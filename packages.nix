{ pkgs, lib, ... }:

{
  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    android-tools
    btrfs-progs
    docker-compose
    git
    zsh

    heroic
    lutris
    protonup
    protonup-qt
    protontricks
    rio
    wine-staging
    winetricks

    usbutils
    pciutils
    bolt
    thunderbolt
    wireguard-tools
  ];

  systemd.packages = with pkgs; [
    bolt
  ];

  programs.kdeconnect.enable = true;

  programs.zsh.enable = true;
  programs.neovim = {
    enable = true;
    defaultEditor = true;
    viAlias = true;
    vimAlias = true;
  };

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    dedicatedServer.openFirewall = true;
  };

  programs.firefox = {
    enable = true;
    nativeMessagingHosts.packages = with pkgs; [
      ff2mpv
    ];
  };

  environment.sessionVariables = {
    MOZ_USE_XINPUT2 = "1";
  };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  services.fwupd.enable = true;
  services.fwupd.extraRemotes = [
    "lvfs-testing"
  ];

  services.flatpak.enable = true;

  xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-gtk ];

  virtualisation.docker.enable = true;
}
