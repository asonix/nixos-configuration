{ name, authorizedKeys ? [] }:

{ pkgs, ... }:

{
  # Define a user account. Don't forget to set a password with `passwd`.
  users.users.asonix = {
    openssh.authorizedKeys.keys = authorizedKeys;
    isNormalUser = true;
    description = name;
    shell = pkgs.zsh;
    extraGroups = [ "lp" "networkmanager" "scanner" "wheel" ];
  };

  users.defaultUserShell = pkgs.zsh;
}
