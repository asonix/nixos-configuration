{ pkgs, ... }:

{
  programs.niri = {
    enable = true;
    package = pkgs.niri;
  };

  environment.systemPackages = with pkgs; [
    alacritty
    brightnessctl
    cage
    fuzzel
    gamescope
    libsecret
    pantheon.pantheon-agent-polkit
    swaybg
    swaylock
    waybar
    wayland-utils
    wl-clipboard
    wlsunset
    xwayland-satellite-unstable
  ];
}
